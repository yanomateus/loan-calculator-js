import {expect, test} from '@jest/globals';
import Loan from '../src/loan.js';


const principal = 1000.0;
const annualInterestRate = 0.5;
const startDate = new Date(2020, 1, 1);
const returnDates = [
    new Date(2020, 1, 2),
    new Date(2020, 1, 3),
    new Date(2020, 1, 4),
    new Date(2020, 1, 5)
];
const gracePeriod = 0;

test('test Loan with very large year size', () => {

    function doAssert(amortizationScheduleType) {
        let yearSize = 100000;
        let loan = new Loan(
            principal,
            annualInterestRate,
            startDate,
            returnDates,
            yearSize,
            gracePeriod,
            amortizationScheduleType
        );

        expect(loan.getAmortizations().map(n => parseFloat(n.toFixed(2)))).toEqual([250.0, 250.0, 250.0, 250.0]);
        expect(loan.getInterestPayments().map(n => parseFloat(n.toFixed(2)))).toEqual([0.0, 0.0, 0.0, 0.0]);
        expect(loan.getDuePayments().map(n => parseFloat(n.toFixed(2)))).toEqual(loan.getAmortizations().map(n => parseFloat(n.toFixed(2))));
    }

    for (let amortizationScheduleType of [
        'progressivePriceSchedule',
        'regressivePriceSchedule',
        'constantAmortizationSchedule'
    ]) {
        doAssert(amortizationScheduleType);
    }
});

test('test unknown amortizations schedule types throws exception', () => {
    expect(() => {
            return new Loan(
                principal,
                annualInterestRate,
                startDate,
                returnDates,
                365,
                gracePeriod,
                'fooo'
            )
        }
    ).toThrow('UnknownAmortizationScheduleType');
})
