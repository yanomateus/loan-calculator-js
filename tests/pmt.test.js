import {expect, test} from '@jest/globals';
import constantReturnPMT from '../src/pmt';

test('check proper constantReturnPMT evaluation', () => {
    expect(constantReturnPMT(1.0, 1.0, [1, 1])).toBeCloseTo(1.0);
});