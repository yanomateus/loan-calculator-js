import {expect, test} from '@jest/globals';
import {ProgressivePriceSchedule, RegressivePriceSchedule} from '../../src/schedule/price';

test('test ProgressivePriceSchedule', () => {

    let principal = 8530.20;
    let dailyInterestRate = 0.03;
    let returnDays = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    let schedule = new ProgressivePriceSchedule(principal, dailyInterestRate, returnDays);

    let expectedAmortizations = [744.09, 766.42, 789.41, 813.09, 837.48, 862.61, 888.49, 915.14, 942.60, 970.87]
    let expectedBalances = [8530.20, 7786.11, 7019.69, 6230.28, 5417.19, 4579.71, 3717.10, 2828.61, 1913.47, 970.87, 0.0];
    let expectedInterests = [255.91, 233.58, 210.59, 186.91, 162.52, 137.39, 111.51, 84.86, 57.40, 29.13];
    let expectedPayments = [1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00]

    expect(schedule.amortizations.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedAmortizations);
    expect(schedule.balance.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedBalances);
    expect(schedule.interestPayments.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedInterests);
    expect(schedule.duePayments.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedPayments);
});

test('test RegressivePriceSchedule', () => {

    let principal = 8530.20;
    let dailyInterestRate = 0.0009857789690617125 // yields 3% every 30 days
    let returnDays = [30, 60, 90, 120, 150, 180, 210, 240, 270, 300]

    let schedule = new RegressivePriceSchedule(principal, dailyInterestRate, returnDays);

    let expectedAmortizations = [970.87, 942.6, 915.14, 888.49, 862.61, 837.48, 813.09, 789.41, 766.42, 744.09];
    let expectedBalances = [8530.20, 7786.11, 7019.69, 6230.28, 5417.19, 4579.71, 3717.10, 2828.61, 1913.47, 970.87, 0.0];
    let expectedInterests = [29.13, 57.4, 84.86, 111.51, 137.39, 162.52, 186.91, 210.59, 233.58, 255.91];
    let expectedPayments = [1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00, 1000.00];

    expect(schedule.amortizations.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedAmortizations);
    expect(schedule.balance.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedBalances);
    expect(schedule.interestPayments.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedInterests);
    expect(schedule.duePayments.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedPayments);
});
