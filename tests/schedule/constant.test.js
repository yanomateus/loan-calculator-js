import {expect, test} from '@jest/globals';
import ConstantAmortizationSchedule from '../../src/schedule/constant';

test('test ConstantAmortizationSchedule', () => {
    let principal = 800.00;
    let dailyInterestRate = 0.8;
    let returnDays = [1, 2, 3, 4, 5];

    let schedule = new ConstantAmortizationSchedule(principal, dailyInterestRate, returnDays);

    let expectedAmortizations = [160, 160, 160, 160, 160];
    let expectedBalances = [800, 640, 480, 320, 160, 0];
    let expectedInterestPayments = [640, 512, 384, 256, 128];
    let expectedPayments = [800, 672, 544, 416, 288];

    expect(schedule.amortizations.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedAmortizations);
    expect(schedule.balance.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedBalances);
    expect(schedule.interestPayments.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedInterestPayments);
    expect(schedule.duePayments.map(n => parseFloat(n.toFixed(2)))).toEqual(expectedPayments);
});
