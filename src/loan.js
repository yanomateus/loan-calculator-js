'use strict';

import ConstantAmortizationSchedule from './schedule/constant';
import {ProgressivePriceSchedule, RegressivePriceSchedule} from './schedule/price';

/**
 * Models a loan
 *
 * @param {Number} principal                The loan principal
 * @param {Number} annualInterestRate       The loan's annual interest rate
 * @param {Date}   startDate                The loan's reference date. This date is usually the one when the borrower
 *                                          signed the loan's contract.
 * @param {Array} returnDates               Array of date objects with the expected return dates. These dates are usually
 *                                          contractually agreed.
 * @param {Number} yearSize                 The reference year size for converting from annual to daily interest rates.
 *                                          (default 365)
 * @param {Number} gracePeriod              The number of days for which the principal is not affected by the
 *                                          capitalization process. (default 0)
 * @param {String} amortizationScheduleType A discriminator string indicating the amortization schedule to be adopted.
 *                                          The available schedules are progressivePriceSchedule,
 *                                          regressivePriceSchedule, ConstantAmortizationSchedule.
 *                                          (default progressivePriceSchedule).
 */
class Loan {

    constructor(
        principal,
        annualInterestRate,
        startDate,
        returnDates,
        yearSize = 365,
        gracePeriod = 0,
        amortizationScheduleType = 'progressivePriceSchedule'
    ) {
        this.principal = principal;
        this.annualInterestRate = annualInterestRate;

        this.dailyInterestRate = (1 + this.annualInterestRate) ** (1.0 / yearSize) - 1;

        this.startDate = startDate;
        this.captalizationStartDate = startDate.getTime() + 1000 * 60 * 60 * 24 * gracePeriod;

        this.returnDates = returnDates;
        this.yearSize = yearSize;
        this.gracePeriod = gracePeriod;

        let amortizationScheduleArgs = [
            this.principal,
            this.dailyInterestRate,
            this.returnDates.map((returnDate) => {
                return (returnDate - this.captalizationStartDate) / (1000 * 60 * 60 * 24);
            })
        ];

        switch (amortizationScheduleType) {
            case 'progressivePriceSchedule':
                this.amortizationSchedule = new ProgressivePriceSchedule(...amortizationScheduleArgs);
                break;
            case 'regressivePriceSchedule':
                this.amortizationSchedule = new RegressivePriceSchedule(...amortizationScheduleArgs);
                break;
            case 'constantAmortizationSchedule':
                this.amortizationSchedule = new ConstantAmortizationSchedule(...amortizationScheduleArgs);
                break;
            default:
                throw 'UnknownAmortizationScheduleType';
        }
    }

    getAmortizations() {
        return this.amortizationSchedule.amortizations;
    }

    getInterestPayments() {
        return this.amortizationSchedule.interestPayments;
    }

    getDuePayments() {
        return this.amortizationSchedule.duePayments;
    }

    getBalance() {
        return this.amortizationSchedule.balance;
    }

}

export default Loan;
