/**
 * Base amortization schedule.
 *
 * Specific amortizations schedules should subclass this one and implement the methods
 *
 * <ul>
 *     <li> setDuePayments </li>
 *     <li> setBalance </li>
 *     <li> setInterestPayments </li>
 *     <li> setAmortizations </li>
 * </ul>
 *
 * @abstract
 *
 * @param {Number} principal         The loan's principal
 * @param {Number} dailyInterestRate The loan's daily interest rate
 * @param {Array} returnDays         Array of integers representing the number
 *                                   of days since the loan was granted until
 *                                   the payments' due dates.
 *
 */
class BaseSchedule {
    constructor(principal, dailyInterestRate, returnDays) {
        this.principal = principal;
        this.dailyInterestRate = dailyInterestRate;
        this.returnDays = returnDays;

        this.amortizations = [];
        this.balance = [];
        this.interestPayments = [];
        this.duePayments = [];

    }

    setDuePayments() {throw 'NotImplemented'}
    setBalance() {throw 'NotImplemented'}
    setInterestPayments() {throw 'NotImplemented'}
    setAmortizations() {throw 'NotImplemented'}

    getTotalDuePayment() {
        return this.duePayments.reduce((agg, currentValue) => agg + currentValue, 0.0);
    }

    getTotalAmortizations() {
        return this.amortizations.reduce((agg, currentValue) => agg + currentValue, 0.0);
    }

    getTotalInterest() {
        return this.interestPayments.reduce((agg, currentValue) => agg + currentValue, 0.0);
    }

}

export default BaseSchedule;
