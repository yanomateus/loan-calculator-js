import BaseSchedule from './base';
import constantReturnPMT from '../pmt';

class BasePriceSchedule extends BaseSchedule {

    constructor(principal, dailyInterestRate, returnDays){
        super(principal, dailyInterestRate, returnDays);
        this.pmt = constantReturnPMT(principal, dailyInterestRate, returnDays);
        this.setBalance();
        this.setDuePayments();
    }

    setBalance() {

        const sumOfReciprocals = this.returnDays.reduce(
            (agg, currentValue) => agg + 1.0 / (1 + this.dailyInterestRate) ** currentValue, 0.0
        );

        this.balance = [0].concat(this.returnDays).map((returnDay) => {
            return (
                this.principal * (1 + this.dailyInterestRate) ** returnDay * (1 - this.returnDays.filter(
                    returnDay_ => returnDay_ <= returnDay
                ).reduce(
                    (agg, currentValue) => agg + 1.0 / (1 + this.dailyInterestRate) ** currentValue, 0.0
                ) / sumOfReciprocals)
            );
        });
    }

    setDuePayments() {
        this.duePayments = this.returnDays.map(_ => this.pmt);
    }
}


export class ProgressivePriceSchedule extends BasePriceSchedule {

    constructor(principal, dailyInterestRate, returnDates) {
        super(principal, dailyInterestRate, returnDates);
        this.setInterestPayments();
        this.setAmortizations();
    }

    setInterestPayments() {
        this.interestPayments = this.returnDays.map((_, i) => {
            return (
                this.pmt
                * (1.0 - 1.0 / (1 + this.dailyInterestRate) ** this.returnDays[this.returnDays.length - i - 1])
            );
        });
    }

    setAmortizations() {
        this.amortizations = this.returnDays.map((_, i) => {
            return this.pmt / (1 + this.dailyInterestRate) ** this.returnDays[this.returnDays.length - i - 1];
        });
    }
}


export class RegressivePriceSchedule extends BasePriceSchedule {

    constructor(principal, dailyInterestRate, returnDays) {
        super(principal, dailyInterestRate, returnDays);
        this.setInterestPayments();
        this.setAmortizations();
    }

    setInterestPayments() {
        this.interestPayments = this.returnDays.map((returnDay) => {
            return this.pmt * (1.0 - 1.0 / (1 + this.dailyInterestRate) ** returnDay);
        });
    }

    setAmortizations() {
        this.amortizations = this.returnDays.map((returnDay) => {
            return this.pmt / (1 + this.dailyInterestRate) ** returnDay;
        });
    }
}
