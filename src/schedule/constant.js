import BaseSchedule from "./base";

/**
 * @class ConstantAmortizationSchedule
 * @extends BaseSchedule
 * @classdesc an amortization schedule is said to be <i>constant</i> if all
 * of its amortizations have the same value. This common value is given by
 * s / k, where s is the principal and k is the number of due payments. Therefore
 * the due payments do not have the same value as in the Price table (or French
 * system), but differ according to how much interest was accumulated over time.
 * If d is the daily interest rate, A_i and J_i are the associated amortization
 * and interest, respectively, and b_i is the balance after the i-th due date. Then
 *
 * <ul>
 *     <li>A_i = s / k</li>
 *     <li>J_i = ((1 + d) ** n_i - (1 + d) ** n_{i - 1}) * (s - A * sum(j: 1 -> i - 1) 1 / (1 + d) ** n_j) </li>
 *     <li>P_i = A + J_i</li>
 *     <li>b_i = s - i * A</li>
 * </ul>
 *
 * @param {Number} principal         Same as {@link BaseSchedule.principal}
 * @param {Number} dailyInterestRate Same as in {@link BaseSchedule.dailyInterestRate}
 * @param {Array}  returnDays        Same as in {@link BaseSchedule.returnDays}
 *
 */
class ConstantAmortizationSchedule extends BaseSchedule {

    constructor(principal, dailyInterestRate, returnDays) {
        super(principal, dailyInterestRate, returnDays);
        this.setBalance();
        this.setDuePayments();
        this.setInterestPayments();
        this.setAmortizations();
    }

    /**
     * Calculate and set the amortization schedule balance
     * @description The balance after each payment is given by
     * b_i = s * (1 - i / k), for all i, 0 <= i <= k, where s is
     * the principal and k is the number of due payments.
     */
    setBalance() {
        this.balance = [...Array(this.returnDays.length + 1).keys()].map(i => {
            return this.principal * (1 - i / this.returnDays.length);
        });
    }

    /**
     * Calculate and set the schedule's amortizations.
     * @description Since this is a constant amortization schedule, all
     * amortizations have the same value, given by s / k, where s is the
     * principal and k is the number of due payments.
     */
    setAmortizations() {
        this.amortizations = this.returnDays.map(_ => this.principal / this.returnDays.length);
    }

    /**
     * Calculate and set the interest part of each due payment.
     * @description The interest is calculated over the last balance and
     * is given by J_i = b_{i-1} * ((1 + d) ** (n_i - n_{i-1}) - 1),
     * where b_{i-1} is the {i-1}-th balance, d is the daily interest
     * rate and n_1, ..., n_k are the return days.
     */
    setInterestPayments() {
        this.interestPayments = [];
        for (let i = 0; i < this.returnDays.length; i++) {
            this.interestPayments.push(
                this.balance[i]
                * ((1 + this.dailyInterestRate) ** (this.returnDays[i] - (this.returnDays[i - 1] || 0)) - 1)
            );
        }
    }

    /**
     * Calculate and set the schedule's due payments.
     * @description In the constant amortization schedule, the due payment
     * (or instalment) value is not fixed as in Price type schedules
     * but depends on how much interest is due for each period and the
     * amortization, which is constant. The due payments are then given by
     * <p>P_i = b_{i-1} * ((1 + d)^{n_i - n_{i-1}} - 1) + s / k,
     * for all i,1 <= i <= k,</p> where b_{i-1} is the {i - 1}-th balance,
     * d is the daily interest rate, s is the principal and
     * n_1, ..., n_k are the return days.
     */
    setDuePayments() {
        this.duePayments = [];
        for (let i = 0; i < this.returnDays.length; i++) {
            this.duePayments.push(
                this.balance[i]
                * ((1 + this.dailyInterestRate) ** (this.returnDays[i] - (this.returnDays[i - 1] || 0)) - 1)
                + this.principal / this.returnDays.length
            );
        }
    }
}

export default ConstantAmortizationSchedule;
