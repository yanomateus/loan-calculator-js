/**
 * constantReturnPMT
 * @description If s is the principal, d is the daily interest rate and
 * n_1, ..., n_k are the numbers of days since the start reference date,
 * then the PMT is given by
 * <p> PMT(s, d, (n_1,...,n_k)) = s / (1 / (1 + d) ** n_1 + ... + 1 / (1 + d) ** n_k). </p>
 * @param {Number} principal         The PMT's principal.
 * @param {Number} dailyInterestRate The PMT's daily interest rate.
 * @param {Array}  returnDays        Array with integers counting the numbers of
 *                                   days since the PMT start until each expected return date.
 */
function constantReturnPMT(principal, dailyInterestRate, returnDays) {
    return principal / returnDays.reduce((total, currentValue) => total + 1.0 / (1 + dailyInterestRate) ** currentValue, 0.0);
}

export default constantReturnPMT;
