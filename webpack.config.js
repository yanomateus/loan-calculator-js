const path = require('path');


module.exports = {
    mode: 'production',
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
        path: path.resolve(__dirname, 'lib'),
        filename: 'loan-calculator.js',
        library: 'loanCalculator',
        libraryTarget: 'umd'
    },
    module: {
        rules: [
            {
                test: '/\.js$/',
                use: {
                    loader: 'babel-loader',
                    options: {
                        preset: ['@babel/preset-env'],
                    }
                }
            }
        ]
    }
};
